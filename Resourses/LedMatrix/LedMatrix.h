#ifndef __LEDMATRIX_H
#define __LEDMATRIX_H

#include "mydeff.h"

//============================== GLOBAL ========================================



void LedMatrix_Init(void);
//============================== STATIC ========================================
static BYTE lineorder;
static BYTE buffer[8][6];        //array[8][7][8] bitlardan iborat, butun ekranni bildiradi. har bir elementi yani BYTEi bitta 595 registr 
static volatile BYTE ledbuffer[7];
static stateSpiEnd stateSpi;
static uint8_t ignoreData;


static WORD shiftOrder;
static WORD PauseOrders[6];
static BYTE shiftBuffer[8][BUFFERSIZE + 1];
static BYTE tempShifted[8][6];


static inline void ShiftLed(unsigned char line);
static inline void SPI2_Tx_DMA(uint8_t *data, int32_t len);
static inline void HorShift(DWORD Delay, WORD shiftColNumber);
static inline WORD UpdateShiftBuffer(void);  	// Gets current time and date from RTC, message to be printed, generates required string and pushes to the shift buffer
static inline void myPrintf(unsigned char *str, WORD size);  	//gets the string(as pointer) with quantity of characters, and pushes to Shift Buffer
static inline BYTE mysprintf(unsigned char* builtSentence, unsigned char* Format, unsigned char* Text, RTC_TimeTypeDef Time, RTC_DateTypeDef Date);
void ResetToDefaultSettings(void);


//============================== MACROS ========================================

//to`g`ridan to`g`ri hardware bufferga jo`natadi
#define SendToBuffer(b)										\
			for(BYTE i = 0 ; i < 8 ; i++)					\
			{												\
				for (BYTE j = 0; j < 6; j++)				\
				{											\
					buffer[i][j] = b[i][j];					\
				}											\
			}
#define SendToShiftBuffer(b,c)								\
			for(BYTE i = 0 ; i < NumRows ; i++)				\
			{												\
				for (BYTE j = 0; j < c; j++)				\
				{											\
					shiftBuffer[i][j] = b[i][j];			\
				}											\
			}
#define CopyArray(s1, s2, Num)								\
			for(BYTE t = 0 ; t < Num ; t++)					\
			{												\
				s1[t] = s2[t];								\
			}	


#endif // !__LEDMATRIX_H
