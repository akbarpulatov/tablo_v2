#include "stm32f1xx.h"
#include "LedMatrixInit.h"
#include "LedMatrix.h"
#include "Font.h"
//------------------------------------------------------
extern volatile Word Flags0;
extern volatile Word Flags1;
extern volatile Word Flags;
//------------------------------------------------------
extern volatile DWORD msTMR1;
extern volatile DWORD msTMR2;
extern volatile DWORD msTMR3;
extern volatile DWORD msTMR4;
extern volatile DWORD msTMR5;
extern volatile DWORD msTMR6;
extern volatile DWORD msTMR7;
extern volatile DWORD msTMR8;
//------------------------------------------------------
unsigned char ucRegSettings[REG_SETTINGS_NREGS];
unsigned char ucRegFormat[REG_FORMAT_NREGS];
unsigned char TextRegister[REG_TEXT_NREGS];
RTC_TimeTypeDef currentTime;
RTC_DateTypeDef currentDate;
signed char temperatureSens = 20;
signed char temperatureMB = 20;
//------------------------------------------------------
void LedMatrix_Init()
{
	ResetToDefaultSettings();
	DMA1_CH_5_Init();
	SPI_2_Init();
	Timer_4_Init();
}
//----------------------------------------------------------
void ResetToDefaultSettings(void)
{
	TemperSource = TemperFromSens;
	shiftTimems = 40;
	
	CopyArray(ucRegFormat, (unsigned char*)"%s %H:%M %t", REG_FORMAT_NREGS);
	CopyArray(TextRegister, (unsigned char*)"TOSHKENT-NAVOI", REG_SETTINGS_NREGS);
	
	FlagTemperMeas = 1;      //Flag for Read Temperature Only once in the beginning of Buffer
}
//------------------------------------------------------
static inline BYTE mysprintf(unsigned char* builtSentence,
	unsigned char* Format,
	unsigned char* Text, 
	RTC_TimeTypeDef Time,
	RTC_DateTypeDef Date)
{
	BYTE i = 0; 
	BYTE k = 0;
	for (; k < NumCols;)
	{
		builtSentence[k++] = ' ';
	}
	
	while (Format[i] != 0x00)
	{
		//Agar simvol '%' bo`lmasa to`g`ridan to`g`ri ko`chirib yozaver        
		if(Format[i] != '%')
		{
			builtSentence[k++] = Format[i++];
		}
		
		//Agar simvol '%' bo`lsa unda keyingi simvolga qarab ish ko`riladi
		else
		{
			i++;
			switch (Format[i++])
			{
			case 's':	
				for (BYTE j = 0; Text[j] != 0x00;)
				{
					builtSentence[k++] = Text[j++];
				} 
				break;
			case 'H':
				PauseOrders[0] = (k * 8) - 6;
				builtSentence[k++] = (Time.Hours / 10) + '0';
				builtSentence[k++] = (Time.Hours % 10) + '0'; 
				break;
			case 'M':
				builtSentence[k++] = (Time.Minutes / 10) + '0';
				builtSentence[k++] = (Time.Minutes % 10) + '0'; break;
			case 'S':
				builtSentence[k++] = (Time.Seconds / 10) + '0';
				builtSentence[k++] = (Time.Seconds % 10) + '0'; break;
			case 'd':
				builtSentence[k++] = (Date.Date / 10) + '0';
				builtSentence[k++] = (Date.Date % 10) + '0'; break;
			case 'm':
				builtSentence[k++] = (Date.Month / 10) + '0';
				builtSentence[k++] = (Date.Month % 10) + '0'; break;
			case 'y':
				builtSentence[k++] = '2';
				builtSentence[k++] = '0';
				builtSentence[k++] = (Date.Year / 10) + '0';
				builtSentence[k++] = (Date.Year % 10) + '0'; break;
			case 't':
				PauseOrders[1] = (k * 8) - 9;
				//Tanlaydigan bo`lishi kerak, ichki temperatura datchikdanmi ko`rsatishi yoki MODBUSdanmi		
				if(TemperSource == TemperFromSens)
				{
					//From Temp Sensor
					builtSentence[k++] = (temperatureSens / 10) + '0';
					builtSentence[k++] = (temperatureSens % 10) + '0'; 
				}
				else
				{
					builtSentence[k++] = (temperatureMB / 10) + '0';
					builtSentence[k++] = (temperatureMB % 10) + '0'; 					
				}
				builtSentence[k++] =  39;
				builtSentence[k++] = 'C'; break;
			}
		}
	}
	
	for (BYTE i = 0; i < NumCols; i++)
	{
		builtSentence[k++] = 0x00;
	}
	builtSentence[k] = 0x00;
	
	return k - 6;
}
//----------------------------------------------------------
static inline WORD UpdateShiftBuffer(void)	// Gets current time and date from RTC, message to be printed, generates required string and pushes to the shift buffer
{
	//ucRegFormat -> SentenChars // SentenChars should be written according to ucRegFormat
	
	WORD SizeOfStrMess;
	
	unsigned char SentenChars[BUFFERSIZE + 1]; 
	
	//	//Getting current time and date
	//	HAL_RTC_GetTime(&hrtc, &currentTime, RTC_FORMAT_BIN);
	//	HAL_RTC_GetDate(&hrtc, &currentDate, RTC_FORMAT_BIN);
	
	SizeOfStrMess = mysprintf(SentenChars, ucRegFormat, TextRegister, currentTime, currentDate);
	myPrintf(SentenChars, SizeOfStrMess);
	return SizeOfStrMess;
}
//------------------------------------------------------
static inline void HorShift(DWORD Delay, WORD shiftColNumber)
{
	
	if (Timer_Shift == 0)
	{
		
		if (Timer_Pause == 0)
		{
		 
			if ((shiftOrder == PauseOrders[0] || shiftOrder == PauseOrders[1]) && shiftOrder != 0)
			{
				Timer_Pause = 3000;
			}
		
			if (shiftOrder >= shiftColNumber * 8) 
			{
				shiftOrder = 0; 
			}
			
			for (BYTE i = 0; i < NumRows; i++)
			{
				for (BYTE j = 0; j < NumCols; j++)
				{
					tempShifted[i][j] = (shiftBuffer[i][(shiftOrder / 8) + j] << (shiftOrder % 8))  |  
										(shiftBuffer[i][(shiftOrder / 8) + j + 1] >> (8 - (shiftOrder % 8)));
				}
			}
			shiftOrder++;
		
			Timer_Shift = Delay;
			SendToBuffer(tempShifted);
		}
	}
}
//----------------------------------------------------------
//Can be used directly print to the Led matrix with SendToBuffer macros
//gets the string(as pointer) with quantity of characters, and pushes to Shift Buffer
static inline void myPrintf(unsigned char *str, WORD size)	
{
	BYTE SentenBuilt[NumRows][BUFFERSIZE];
	for (WORD n = 0; n < size + 6; n++)
	{
		//SentenBuiltga qo`shilib boradi BYTE ma BYTE
		for(BYTE i = 0 ; i < NumRows ; i++)
		{
			SentenBuilt[i][n] = Fonts_LMS[(str[n])][i];
		}
	}
	//SendToBuffer(SentenBuilt);
	SendToShiftBuffer(SentenBuilt, size + 6);
}
//------------------------------------------------------
void TIM4_IRQHandler(void)
{
	TIM4->SR &= ~TIM_SR_UIF;
	
	if (lineorder % 8 == 0)
	{
		HorShift(shiftTimems, UpdateShiftBuffer());
	}
	ShiftLed((lineorder++) % 8);
}
//------------------------------------------------------
static inline void ShiftLed(BYTE line)
{
	//Setting up the Sending Buffer according to the Buffer of the Display
	// Qator tanlandi, binaryda 1 turgan o`rin qator nomeri
	ledbuffer[6] = ~(1 << line); 
	for (BYTE i = 0; i < 6; i++)
	{
		ledbuffer[i] = ~buffer[line][i];
	}
	SPI2_Tx_DMA((uint8_t*)ledbuffer, 7);
}
//------------------------------------------------------
static inline void SPI2_Tx_DMA(uint8_t *data, int32_t len)
{
	DMA1_Channel5->CCR &= ~(DMA_CCR_EN);
	DMA1_Channel5->CMAR = (uint32_t)data;
	DMA1_Channel5->CNDTR = len;
	DMA1_Channel5->CCR |= DMA_CCR_EN;
	RCLK = 0;
	SPI2->CR2 |= SPI_CR2_TXDMAEN;
}
//------------------------------------------------
void DMA1_Channel5_IRQHandler(void)
{
	if (DMA1->ISR & DMA_ISR_TCIF5)
	{
		//spi tx dma disable(for not retransferring data)
		SPI2->CR2 &= ~SPI_CR2_TXDMAEN;  
		//interrupt flag clear
		DMA1->IFCR |= DMA_IFCR_CTCIF5;      
		ignoreData = SPI2->DR;
		stateSpi = PRELASTBYTE;
		SPI2->CR2 |= SPI_CR2_RXNEIE;
	}
}
//------------------------------------------------
void SPI2_IRQHandler(void)
{
	if (SPI2->SR & SPI_SR_RXNE)
	{
		switch (stateSpi)
		{
		case PRELASTBYTE:
			ignoreData = SPI2->DR;
			stateSpi = LASTBYTE;
			break;
		case LASTBYTE:	
			RCLK ^= 1;
			SPI2->CR2 &= ~(SPI_CR2_RXNEIE);
			break;
				
		}
	}
}
//------------------------------------------------


