#include "stm32f1xx.h"
#include "LedMatrixInit.h"
//------------------------------------------------------
void DMA1_CH_5_Init(void)
{
	RCC->AHBENR |= RCC_AHBENR_DMA1EN;
	
	DMA1_Channel5->CPAR = (uint32_t)(&(SPI2->DR));
	DMA1_Channel5->CCR |= (0b10 << DMA_CCR_PL_Pos);
	DMA1_Channel5->CCR |= (1 << DMA_CCR_DIR_Pos)
					   |  (0 << DMA_CCR_CIRC_Pos)
					   |  (0 << DMA_CCR_PINC_Pos)
					   |  (1 << DMA_CCR_MINC_Pos)
					   |  (0b10 << DMA_CCR_PSIZE_Pos)
					   |  (0b00 << DMA_CCR_MSIZE_Pos)
					   |  (1 << DMA_CCR_TCIE_Pos)
					   |  (0 << DMA_CCR_TEIE_Pos);
	NVIC_EnableIRQ(DMA1_Channel5_IRQn);
}
//------------------------------------------------------
void SPI_2_Init(void)
{
	RCC->APB1ENR |= RCC_APB1ENR_SPI2EN;
	RCC->APB2ENR |= RCC_APB2ENR_IOPAEN
				 |  RCC_APB2ENR_IOPBEN;
	
	//PB13
	GPIOB->CRH &= ~(GPIO_CRH_CNF13_Msk | GPIO_CRH_MODE13_Msk);
	GPIOB->CRH |= (0b11 << GPIO_CRH_MODE13_Pos) | (0b10 << GPIO_CRH_CNF13_Pos);
	
	//PB15
	GPIOB->CRH &= ~(GPIO_CRH_CNF15_Msk | GPIO_CRH_MODE15_Msk);
	GPIOB->CRH |= (0b11 << GPIO_CRH_MODE15_Pos) | (0b10 << GPIO_CRH_CNF15_Pos);
	
	//PA8
	GPIOA->CRH &= ~(GPIO_CRH_CNF8_Msk | GPIO_CRH_MODE8_Msk);
	GPIOA->CRH |= (0b11 << GPIO_CRH_MODE8_Pos) | (0b00 << GPIO_CRH_CNF8_Pos);
	
	//SPI2 Configuration
	SPI2->CR1 |= (0b010 << SPI_CR1_BR_Pos)
			  |  (0 << SPI_CR1_CPOL_Pos)
			  |  (0 << SPI_CR1_CPHA_Pos)
			  |  (0 << SPI_CR1_DFF_Pos)
			  |  (0 << SPI_CR1_LSBFIRST_Pos)
			  |  (1 << SPI_CR1_SSM_Pos)
			  |  (1 << SPI_CR1_SSI_Pos)
			  |  (1 << SPI_CR1_MSTR_Pos)
			  |  (1 << SPI_CR1_SPE_Pos);
	NVIC_EnableIRQ(SPI2_IRQn);
}
//------------------------------------------------------
void Timer_4_Init(void)
{
	//Timer 4 Init
	//Peripheral Clock Enable
	RCC->APB1ENR |= RCC_APB1ENR_TIM4EN;
	while (!(RCC->APB1ENR & RCC_APB1ENR_TIM4EN)) ;
	NVIC_EnableIRQ(TIM4_IRQn);
	TIM4->PSC = 100 - 1;
	TIM4->ARR = 1200 - 1;
	
	//Update Interrupt Enable
	TIM4->DIER |= TIM_DIER_UIE;
	
	//Counter Enable
	TIM4->CR1 |= TIM_CR1_CEN;
}