#ifndef __SPI_H_
#define __SPI_H_


#include "mydeff.h"
#include "stm32f1xx.h"

void SPIx_Init(SPI_TypeDef* SPIx);
void SPI_SendByte(uint8_t data);
uint8_t SPI_ReceiveByte(void);
uint8_t SPIx_WriteRead(uint8_t data);


#endif /* __SPI_H_ */
