#include "tcp.h"
#include "mydeff.h"
#include <stdio.h>
#include "UART.h"
//--------------------------------------------------
//extern UART_HandleTypeDef huart1;
//-----------------------------------------------
extern volatile char str1[60];
extern uint8_t str1_len;
extern uint8_t net_buf[ENC28J60_MAXFRAME];
extern uint8_t macaddr[6];
extern uint8_t ipaddr[4];

//extern uint8_t eMBRegHoldingCB(uint16_t* pucRegBuffer, uint16_t usAdress, uint16_t usNRegs, uint8_t eMode);

uint8_t eMBRegHoldingCB(uint16_t* pucRegBuffer, uint16_t usAdress, uint16_t usNRegs, uint8_t eMode)
{
#ifdef myDEBUG
	str1_len = sprintf((char*)str1, "\r\nAD:%d;NR:%dMO%d\r\n", usAdress, usNRegs, eMode);
//	HAL_UART_Transmit(&huart1, (uint8_t*)str1, strlen(str1), 0x1000);
#endif // myDEBUG
	
	uint16_t usNBytes = usNRegs;
	uint8_t    eStatus = 0;
	
	
	if (eMode == 0x10)
	{
		int iRegIndex;
        
		//Time address implementation
		if((usAdress >= REG_TIME_START) && (usAdress + usNBytes <= REG_TIME_START + REG_TIME_NREGS))
		{
			iRegIndex = (int)(usAdress - REG_TIME_START);
			while (usNBytes-- > 0)
			{
//				TimeDateRegister[iRegIndex++] = (uint8_t)*pucRegBuffer++;
			}
//			currentTime.Hours = TimeDateRegister[0];
//			currentTime.Minutes = TimeDateRegister[1];
//			currentTime.Seconds = TimeDateRegister[2];
//			HAL_RTC_SetTime(&hrtc, &currentTime, RTC_FORMAT_BIN);
//    
//			currentDate.Date = TimeDateRegister[3];
//			currentDate.Month = TimeDateRegister[4];
//			currentDate.Year = TimeDateRegister[5];
//			HAL_RTC_SetDate(&hrtc, &currentDate, RTC_FORMAT_BIN);
		}
        
		//Text address implementation
		else if((usAdress >= REG_TEXT_START) && (usAdress + usNBytes <= REG_TEXT_START + REG_TEXT_NREGS))
		{
			iRegIndex = (int)(usAdress - REG_TEXT_START);
			while (usNBytes-- > 0)
			{
//				TextRegister[iRegIndex++] = (uint8_t)*pucRegBuffer++;
			}
//			TextRegister[iRegIndex++] = NULL;
		}
        
		//Format address implementation
		else if((usAdress >= REG_FORMAT_START) && (usAdress + usNBytes <= REG_FORMAT_START + REG_FORMAT_NREGS))
		{
			iRegIndex = (int)(usAdress - REG_FORMAT_START);
			while (usNBytes-- > 0)
			{
//				ucRegFormat[iRegIndex++] = (uint8_t)*pucRegBuffer++;
			}
//			ucRegFormat[iRegIndex++] = NULL;
		}
        
		//Settings address implementation
		else if((usAdress >= REG_SETTINGS_START) && (usAdress + usNBytes <= REG_SETTINGS_START + REG_SETTINGS_NREGS))
		{
			iRegIndex = (int)(usAdress - REG_SETTINGS_START);
			while (usNBytes-- > 0)
			{
//				ucRegSettings[iRegIndex++] = (uint8_t)*pucRegBuffer++;
			}
//			ucRegSettings[iRegIndex++] = NULL;
		}
		//Temperature address implementation
		else if((usAdress >= REG_TEMP_START) && (usAdress + usNBytes <= REG_TEMP_START + REG_TEMP_NREGS))
		{
			iRegIndex = (int)(usAdress - REG_TEMP_START);
			while (usNBytes-- > 0)
			{
//				ucRegSettings[iRegIndex++] = (uint8_t)*pucRegBuffer++;
			}
//			ucRegSettings[iRegIndex++] = NULL;
		}
		else
		{
			eStatus = 1;
		}
        
	}
    
	return eStatus;
}
//--------------------------------------------------
uint8_t modbus_read(enc28j60_frame_ptr *frame, uint16_t len)
{
	uint8_t res = 0;
	ip_pkt_ptr *ip_pkt = (void*)(frame->data);
	tcp_pkt_ptr *tcp_pkt = (void*)(ip_pkt->data);
	modbus_receive_pkt_ptr *modbus_rec_pkt = (void*)(tcp_pkt->data);

	modbus_rec_pkt->Transaction_ID = be16toword(modbus_rec_pkt->Transaction_ID);
	modbus_rec_pkt->Message_len = be16toword(modbus_rec_pkt->Message_len);
	modbus_rec_pkt->FirstRegister = be16toword(modbus_rec_pkt->FirstRegister);
	modbus_rec_pkt->NumberOfRegisters = be16toword(modbus_rec_pkt->NumberOfRegisters);
	
	eMBRegHoldingCB(modbus_rec_pkt->data,
		modbus_rec_pkt->FirstRegister, 
		modbus_rec_pkt->NumberOfRegisters,
		modbus_rec_pkt->Function_Code);

	//Answer to modbus
	//modbus_write(frame, len, MODBUSANSWER);
	return res;
}
//--------------------------------------------------
uint8_t tcp_send(uint8_t *ip_addr, uint16_t port, uint8_t op)
{
	uint16_t sz_data = 0;
	uint8_t res = 0;
	uint16_t len = 0;
	static uint32_t num_seg = 0;
	enc28j60_frame_ptr *frame = (void*) net_buf;
	ip_pkt_ptr *ip_pkt = (void*)(frame->data);
	tcp_pkt_ptr *tcp_pkt = (void*)(ip_pkt->data);
	if (op == TCP_OP_SYNACK)
	{
		//�������� ��������� ������ TCP
		tcp_pkt->port_dst = be16toword(port);
		tcp_pkt->port_src = be16toword(LOCAL_PORT_TCP);
		tcp_pkt->num_ask = be32todword(be32todword(tcp_pkt->bt_num_seg) + 1);
		tcp_pkt->bt_num_seg = rand();
		tcp_pkt->fl = TCP_SYN | TCP_ACK;
		tcp_pkt->size_wnd = be16toword(8192);
		tcp_pkt->urg_ptr = 0;
		len = sizeof(tcp_pkt_ptr) + 4;
		tcp_pkt->len_hdr = len << 2;
		tcp_pkt->data[0] = 2;   //Maximum Segment Size (2)
		tcp_pkt->data[1] = 4;   //Length
		tcp_pkt->data[2] = 0x05;
		tcp_pkt->data[3] = 0x82;
		tcp_pkt->cs = 0;
		tcp_pkt->cs = checksum((uint8_t*)tcp_pkt - 8, len + 8, 2);
		//�������� ��������� ������ IP
		str1_len = sprintf((char*)str1, "len:%d\r\n", len);
		Uart_Send_Dma((uint8_t*)str1, str1_len);
		len += sizeof(ip_pkt_ptr);
		str1_len = sprintf((char*)str1, "len:%d\r\n", len);
		Uart_Send_Dma((uint8_t*)str1, str1_len);
		ip_pkt->len = be16toword(len);
		ip_pkt->id = 0;
		ip_pkt->ts = 0;
		ip_pkt->verlen = 0x45;
		ip_pkt->fl_frg_of = 0;
		ip_pkt->ttl = 128;
		ip_pkt->cs = 0;
		ip_pkt->prt = IP_TCP;
		memcpy(ip_pkt->ipaddr_dst, ip_addr, 4);
		memcpy(ip_pkt->ipaddr_src, ipaddr, 4);
		ip_pkt->cs = checksum((void*)ip_pkt, sizeof(ip_pkt_ptr), 0);
		//�������� ��������� Ethernet
		memcpy(frame->addr_dest, frame->addr_src, 6);
		memcpy(frame->addr_src, macaddr, 6);
		frame->type = ETH_IP;
		len += sizeof(enc28j60_frame_ptr);
		enc28j60_packetSend((void*)frame, len);
		str1_len = sprintf((char*)str1, "len:%d\r\n", len);
		Uart_Send_Dma((uint8_t*)str1, str1_len);
		Uart_Send_Dma((uint8_t*)"SYN ACK\r\n", 9);
		//HAL_UART_Transmit(&huart1, (uint8_t*)"SYN ACK\r\n", 9, 0x1000);
	}
	else if (op == TCP_OP_ACK_OF_FIN)
	{
		//�������� ��������� ������ TCP
		tcp_pkt->port_dst = be16toword(port);
		tcp_pkt->port_src = be16toword(LOCAL_PORT_TCP);
		num_seg = tcp_pkt->num_ask;
		tcp_pkt->num_ask = be32todword(be32todword(tcp_pkt->bt_num_seg) + 1);
		//��������� 0 � USART, ����� ��������� ���
		Uart_Send_Dma((uint8_t*)0, 1);
		tcp_pkt->bt_num_seg = num_seg;
		tcp_pkt->fl = TCP_ACK;
		tcp_pkt->size_wnd = be16toword(8192);
		tcp_pkt->urg_ptr = 0;
		len = sizeof(tcp_pkt_ptr);
		tcp_pkt->len_hdr = len << 2;
		tcp_pkt->cs = 0;
		tcp_pkt->cs = checksum((uint8_t*)tcp_pkt - 8, len + 8, 2);
		//�������� ��������� ������ IP
		str1_len = sprintf((char*)str1, "len:%d\r\n", len);
		Uart_Send_Dma((uint8_t*)str1, str1_len);
		len += sizeof(ip_pkt_ptr);
		str1_len = sprintf((char*)str1, "len:%d\r\n", len);
		Uart_Send_Dma((uint8_t*)str1, str1_len);
		ip_pkt->len = be16toword(len);
		ip_pkt->id = 0;
		ip_pkt->ts = 0;
		ip_pkt->verlen = 0x45;
		ip_pkt->fl_frg_of = 0;
		ip_pkt->ttl = 128;
		ip_pkt->cs = 0;
		ip_pkt->prt = IP_TCP;
		memcpy(ip_pkt->ipaddr_dst, ip_addr, 4);
		memcpy(ip_pkt->ipaddr_src, ipaddr, 4);
		ip_pkt->cs = checksum((void*)ip_pkt, sizeof(ip_pkt_ptr), 0);
		//�������� ��������� Ethernet
		memcpy(frame->addr_dest, frame->addr_src, 6);
		memcpy(frame->addr_src, macaddr, 6);
		frame->type = ETH_IP;
		len += sizeof(enc28j60_frame_ptr);
		enc28j60_packetSend((void*)frame, len);
		Uart_Send_Dma((uint8_t*)"\r\nACK OF FIN", 12);
		//		HAL_UART_Transmit(&huart1, (uint8_t*)"ACK OF FIN\r\n", 12, 0x1000);
				tcp_pkt->fl = TCP_FIN | TCP_ACK;
		len = sizeof(tcp_pkt_ptr);
		tcp_pkt->cs = 0;
		tcp_pkt->cs = checksum((uint8_t*)tcp_pkt - 8, len + 8, 2);
		len += sizeof(ip_pkt_ptr);
		len += sizeof(enc28j60_frame_ptr);
		enc28j60_packetSend((void*)frame, len);
	}
	else if (op == TCP_OP_ACK_OF_DATA)
	{
		//�������� ��������� ������ TCP
		sz_data = be16toword(ip_pkt->len) - 20 - (tcp_pkt->len_hdr >> 2);
		tcp_pkt->port_dst = be16toword(port);
		tcp_pkt->port_src = be16toword(LOCAL_PORT_TCP);
		num_seg = tcp_pkt->num_ask;
		tcp_pkt->num_ask = be32todword(be32todword(tcp_pkt->bt_num_seg) + sz_data);
		str1_len = sprintf((char*)str1, "sz_data:%u\r\n", sz_data);
		Uart_Send_Dma((uint8_t*)str1, str1_len);
		tcp_pkt->bt_num_seg = num_seg;
		tcp_pkt->fl = TCP_ACK;
		tcp_pkt->size_wnd = be16toword(8192);
		tcp_pkt->urg_ptr = 0;
		len = sizeof(tcp_pkt_ptr);
		tcp_pkt->len_hdr = len << 2;
		tcp_pkt->cs = 0;
		tcp_pkt->cs = checksum((uint8_t*)tcp_pkt - 8, len + 8, 2);
		//�������� ��������� ������ IP
		str1_len = sprintf((char*)str1, "len:%d\r\n", len);
		Uart_Send_Dma((uint8_t*)str1, str1_len);
		len += sizeof(ip_pkt_ptr);
		str1_len = sprintf((char*)str1, "len:%d\r\n", len);
		Uart_Send_Dma((uint8_t*)str1, str1_len);
		ip_pkt->len = be16toword(len);
		ip_pkt->id = 0;
		ip_pkt->ts = 0;
		ip_pkt->verlen = 0x45;
		ip_pkt->fl_frg_of = 0;
		ip_pkt->ttl = 128;
		ip_pkt->cs = 0;
		ip_pkt->prt = IP_TCP;
		memcpy(ip_pkt->ipaddr_dst, ip_addr, 4);
		memcpy(ip_pkt->ipaddr_src, ipaddr, 4);
		ip_pkt->cs = checksum((void*)ip_pkt, sizeof(ip_pkt_ptr), 0);
		//�������� ��������� Ethernet
		memcpy(frame->addr_dest, frame->addr_src, 6);
		memcpy(frame->addr_src, macaddr, 6);
		frame->type = ETH_IP;
		len += sizeof(enc28j60_frame_ptr);
		enc28j60_packetSend((void*)frame, len);
		
#ifdef myDEBUG
		//obrabotkani shu yerda qilamiz
		if((tcp_pkt->data[2] == 0 && tcp_pkt->data[3] == 0))
		{
			//			HAL_UART_Transmit(&huart1, (uint8_t*)("Modbus packet received\r\n"), 24, 0x1000);
						Uart_Send_Dma((uint8_t*)"Modbus packet received\r\n", 24);
			
			modbus_read(frame, len);
#ifdef TEST
			modbus_resp_pkt_ptr* modbus_resp_pkt = (void*)(tcp_pkt->data);
			
			modbus_resp_pkt->Transaction_ID = be16toword(modbus_resp_pkt->Transaction_ID);
			modbus_resp_pkt->Message_len = be16toword(6);
			modbus_resp_pkt->FirstRegister = be16toword(modbus_resp_pkt->FirstRegister);
			modbus_resp_pkt->NumberOfRegisters = be16toword(modbus_resp_pkt->NumberOfRegisters);
			
			tcp_pkt->fl = TCP_ACK | TCP_PSH;
			len = sizeof(tcp_pkt_ptr);
			tcp_pkt->len_hdr = len << 2;			
			len += 12;
			tcp_pkt->cs = 0;
			tcp_pkt->cs = checksum((uint8_t*)tcp_pkt - 8, len + 8, 2);
			//�������� ��������� ������ IP
			len += sizeof(ip_pkt_ptr);
			ip_pkt->len = be16toword(len);
			ip_pkt->cs = 0;
			ip_pkt->cs = checksum((void*)ip_pkt, sizeof(ip_pkt_ptr), 0);
			len += sizeof(enc28j60_frame_ptr);
			enc28j60_packetSend((void*)frame, len);
			
#endif // TEST	
			
		} else
#endif
		//���� ������ "Hello!!!", �� �������� �����
		 if(!strcmp((char*)tcp_pkt->data, "Hello!!!"))
		{
			strcpy((char*)tcp_pkt->data, "Hello to TCP Client!!!\r\n");
			tcp_pkt->fl = TCP_ACK | TCP_PSH;
			str1_len = sprintf((char*)str1, "hdr_len:%d\r\n", sizeof(tcp_pkt_ptr));
			Uart_Send_Dma((uint8_t*)str1, str1_len);
			len = sizeof(tcp_pkt_ptr);
			tcp_pkt->len_hdr = len << 2;
			len += strlen((char*)tcp_pkt->data);
			tcp_pkt->cs = 0;
			tcp_pkt->cs = checksum((uint8_t*)tcp_pkt - 8, len + 8, 2);
			//�������� ��������� ������ IP
			len += sizeof(ip_pkt_ptr);
			ip_pkt->len = be16toword(len);
			ip_pkt->cs = 0;
			ip_pkt->cs = checksum((void*)ip_pkt, sizeof(ip_pkt_ptr), 0);
			len += sizeof(enc28j60_frame_ptr);
			enc28j60_packetSend((void*)frame, len);
		}
		

	}
	return res;
}
//--------------------------------------------------

uint8_t tcp_read(enc28j60_frame_ptr *frame, uint16_t len)
{
	uint8_t res = 0;
	uint16_t len_data = 0;
	uint16_t i = 0;
	ip_pkt_ptr *ip_pkt = (void*)(frame->data);
	tcp_pkt_ptr *tcp_pkt = (void*)(ip_pkt->data);
	len_data = be16toword(ip_pkt->len) - 20 - (tcp_pkt->len_hdr >> 2);
	
	str1_len = sprintf((char*)str1,
		"%d.%d.%d.%d-%d.%d.%d.%d %d tcp\r\n",
		ip_pkt->ipaddr_src[0],
		ip_pkt->ipaddr_src[1],
		ip_pkt->ipaddr_src[2],
		ip_pkt->ipaddr_src[3],
		ip_pkt->ipaddr_dst[0],
		ip_pkt->ipaddr_dst[1],
		ip_pkt->ipaddr_dst[2],
		ip_pkt->ipaddr_dst[3],
		len_data);
	Uart_Send_Dma((uint8_t*)str1, str1_len);

	//if we have data, it will show them on the terminal programmd
	if(len_data)
	{
		//		uint8_t tempNum[2];
		//		for (i=0;i<len_data;i++)
		//		{
		//			tempNum[0]= tcp_pkt->data[i];
		//			HAL_UART_Transmit(&huart1,tempNum,1,0x1000);
		//		}
			Uart_Send_Dma((uint8_t*)"\r\n", 2);
		//				HAL_UART_Transmit(&huart1, (uint8_t*)"\r\n", 2, 0x1000);
			uint8_t tempLen;
		tempLen = sprintf((char*)str1,
			"data=%02X%02X %02X%02X %02X%02X %02X%02X\r\n",
			tcp_pkt->data[0], 
			tcp_pkt->data[1],
			tcp_pkt->data[2],
			tcp_pkt->data[3],
			tcp_pkt->data[4],
			tcp_pkt->data[5], 
			tcp_pkt->data[6],
			tcp_pkt->data[7]);
		Uart_Send_Dma((uint8_t*)str1, tempLen);
		//HAL_UART_Transmit(&huart1, (uint8_t*)str1, tempLen, 0x1000);
		//���� ������� ���� �������������, �� ���������� ���� ������
		if(tcp_pkt->fl&TCP_ACK)
		{
			tcp_send(ip_pkt->ipaddr_src, be16toword(tcp_pkt->port_src), TCP_OP_ACK_OF_DATA);
		}
	}
	if (tcp_pkt->fl == TCP_SYN)
	{
		tcp_send(ip_pkt->ipaddr_src, be16toword(tcp_pkt->port_src), TCP_OP_SYNACK);
	}
	else if (tcp_pkt->fl == (TCP_FIN | TCP_ACK))
	{
		tcp_send(ip_pkt->ipaddr_src, be16toword(tcp_pkt->port_src), TCP_OP_ACK_OF_FIN);
	}
	else if (tcp_pkt->fl == (TCP_PSH | TCP_ACK))
	{
		//if no data
		if(!len_data)
		{
			tcp_send(ip_pkt->ipaddr_src, be16toword(tcp_pkt->port_src), TCP_OP_ACK_OF_FIN);
		}
	}
	else if (tcp_pkt->fl == TCP_ACK)
	{
		Uart_Send_Dma((uint8_t*)"ACK\r\n", 5);
//		HAL_UART_Transmit(&huart1, (uint8_t*)"ACK\r\n", 5, 0x1000);
	}
	return res;
}
//--------------------------------------------------
