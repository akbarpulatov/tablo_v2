#ifndef __I2C_H
#define __I2C_H

#include "mydeff.h"

void i2c_Init(void);

void I2C_Read(uint8_t reg_addr, uint8_t *data, uint8_t len);
void I2C_Write(uint8_t reg_addr, uint8_t *data, uint8_t len);


#endif // !__I2C_H
