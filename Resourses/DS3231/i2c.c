#include "stm32f1xx.h"
#include "i2c.h"
//------------------------------------------------
#define ADR	0xD0
#define I2C_MODE_WRITE	0
#define I2C_MODE_READ	1
#define I2C_ADDRESS(n,m)	((0xFE & n) | m & 0x01)

void i2c_write(void);

extern volatile	Word Flags0;
extern volatile	Word Flags1;
extern volatile	Word Flags;

extern volatile DS_3231_typedef DS_3231;

//------------------------------------------------
volatile typedef_i2c my_i2c;
uint8_t tempp;
uint8_t tempp1[10];
//------------------------------------------------
void i2c_Init(void)
{
	I2CREADFLAG = 0;
	
	RCC->APB1ENR |= RCC_APB1ENR_I2C1EN;
	RCC->APB2ENR |= RCC_APB2ENR_IOPBEN
				 |  RCC_APB2ENR_AFIOEN;
	
	//SCL = PB6 = AFOD
	GPIOB->CRL &= ~(GPIO_CRL_CNF6 | GPIO_CRL_MODE6_Msk);
	GPIOB->CRL |= (0b11 << GPIO_CRL_MODE6_Pos) | (0b11 << GPIO_CRL_CNF6_Pos);

	//SDA = PB7 = AFOD
	GPIOB->CRL &= ~(GPIO_CRL_CNF7 | GPIO_CRL_MODE7_Msk);
	GPIOB->CRL |= (0b11 << GPIO_CRL_MODE7_Pos) | (0b11 << GPIO_CRL_CNF7_Pos);
	
	
	//I2C1
	I2C1->CR1 &= ~I2C_CR1_SMBUS;
	I2C1->CR2 |= 36 << I2C_CR2_FREQ_Pos
			  | 1 << I2C_CR2_DMAEN_Pos
			  | 0 << I2C_CR2_LAST_Pos;
	I2C1->CCR &= ~(I2C_CCR_FS | I2C_CCR_DUTY);
	I2C1->CCR &= ~I2C_CCR_CCR;
	I2C1->CCR |= 180 << I2C_CCR_CCR_Pos;
	I2C1->TRISE &= ~I2C_TRISE_TRISE;
	I2C1->TRISE |= 37 << I2C_TRISE_TRISE_Pos;
	I2C1->CR1 |= I2C_CR1_PE;
	
	I2C1->CR1 |= I2C_CR1_ACK;
	//I2C1->CR2 |= I2C_CR2_ITEVTEN;
	//NVIC_EnableIRQ(I2C1_EV_IRQn);
}
//------------------------------------------------
void I2C_Write(uint8_t reg_addr, uint8_t *data, uint8_t len)
{
	uint8_t i = 0;
	//Start
	I2C1->CR1 |= I2C_CR1_START;		
	while (!(I2C1->SR1 & I2C_SR1_SB)) ;
	(void) I2C1->SR1;
		
	//send the address of device
	I2C1->DR = I2C_ADDRESS(ADR, I2C_MODE_WRITE);
	while (!(I2C1->SR1 & I2C_SR1_ADDR)) ;
	(void) I2C1->SR1;
	(void) I2C1->SR2;
		
	//send the address of the register
	I2C1->DR = reg_addr;
	while (!(I2C1->SR1 & I2C_SR1_TXE)) ;
	while (len--)
	{
		//send data one by one
		I2C1->DR = data[i++];
		while (!(I2C1->SR1 & I2C_SR1_BTF)) ;	
	}
	I2C1->CR1 |= I2C_CR1_STOP;		
}

void I2C_Read(uint8_t reg_addr, uint8_t *data, uint8_t len)
{
	
	uint8_t i = 0;
	I2C1->CR1 |= I2C_CR1_ACK;
	//start
	I2C1->CR1 |= I2C_CR1_START;		
	while (!(I2C1->SR1 & I2C_SR1_SB)) ;
	(void) I2C1->SR1;

	//send the address of device
	I2C1->DR = I2C_ADDRESS(ADR, I2C_MODE_WRITE);
	while (!(I2C1->SR1 & I2C_SR1_ADDR)) ;
	(void) I2C1->SR1;
	(void) I2C1->SR2;

	//send the register address
	I2C1->DR = reg_addr;
	while (!(I2C1->SR1 & I2C_SR1_TXE)) ;	
	I2C1->CR1 |= I2C_CR1_STOP;	
			
	//restart for reading
	I2C1->CR1 |= I2C_CR1_START;		
	while (!(I2C1->SR1 & I2C_SR1_SB)) ;
	(void) I2C1->SR1;
		
	//send the device address with reading command
	I2C1->DR = I2C_ADDRESS(ADR, I2C_MODE_READ);
	while (!(I2C1->SR1 & I2C_SR1_ADDR)) ;
	(void) I2C1->SR1;
	(void) I2C1->SR2;
	len--;
	while (len--)
	{
		while (!(I2C1->SR1 & I2C_SR1_RXNE)) ;
		data[i++] = I2C1->DR;	
	}
	//read the Data Register	
	I2C1->CR1 &= ~I2C_CR1_ACK;
	while (!(I2C1->SR1 & I2C_SR1_RXNE)) ;
	data[i] = I2C1->DR;	
	I2C1->CR1 |= I2C_CR1_STOP;
}
//------------------------------------------------


