#ifndef __DS3231_H
#define __DS3231_H

#include "mydeff.h"

//REGISTER ADDRESSES
#define DS3231_ADDRESS_TIME 0x00
#define DS3231_ADDRESS_DATE 0x04

#define DS3231_ADDRESS_CONTROL_REGISTER 0x0E
#define DS3231_ADDRESS_STATUS_REGISTER 0x0F

#define DS3231_FLAG_STATUS_A1F (1 << 0)
#define DS3231_FLAG_STATUS_A2F (1 << 1)

#define DS3231_FLAG_CONTROL_A1IE (1 << 0)
#define DS3231_FLAG_CONTROL_A2IE (1 << 1)
#define DS3231_FLAG_CONTROL_INTCN (1 << 2)
#define DS3231_FLAG_CONTROL_RS1 (1 << 3)
#define DS3231_FLAG_CONTROL_RS2 (1 << 4)
#define DS3231_FLAG_CONTROL_CONV (1 << 5)
#define DS3231_FLAG_CONTROL_BBSQW (1 << 6)
#define DS3231_FLAG_CONTROL_EOSC (1 << 7)

#define DS3231_ALARM_NO_MATCH 0x50
#define DS3231_ALARM_MATCH 0x00
#define DS3231_ALARM_DAY_MATCH 0x28// DY
#define DS3231_ALARM_DATE_MATCH 0x00// DT



void ds3231_Init(void);

void ds3231_ReadTime(RTC_TimeTypeDef*);
void ds3231_WriteTime(RTC_TimeTypeDef*);
void ds3231_ReadDate(RTC_DateTypeDef*);
void ds3231_WriteDate(RTC_DateTypeDef*);





#endif // !__DS3231_H
