#include "stm32f1xx.h"
#include "DS3231.h"
#include "i2c.h"
//------------------------------------------------
extern volatile	Word Flags0;
extern volatile	Word Flags1;
extern volatile	Word Flags;
extern volatile typedef_i2c my_i2c;
//Stores Actual Values of Registers, is updated by DMA, when calling "ds3231_Read" function
extern volatile DS_3231_typedef DS_3231;
//------------------------------------------------
volatile RTC_DateTypeDef Date;
volatile RTC_TimeTypeDef Time;
//------------------------------------------------
void ds3231_Init(void)
{
	i2c_Init();
}
//------------------------------------------------
void ds3231_Reinit(void)
{
	
}
//------------------------------------------------
void ds3231_ReadTime(RTC_TimeTypeDef *l_Time)
{
	I2C_Read(DS3231_ADDRESS_TIME, (uint8_t*)l_Time, 3);
}
//------------------------------------------------
void ds3231_WriteTime(RTC_TimeTypeDef *l_Time)
{
	I2C_Write(DS3231_ADDRESS_TIME, (uint8_t*)l_Time, 3);
}
//------------------------------------------------
void ds3231_ReadDate(RTC_DateTypeDef *l_Date)
{
	I2C_Read(DS3231_ADDRESS_DATE, (uint8_t*)l_Date, 3);
}
//------------------------------------------------
void ds3231_WriteDate(RTC_DateTypeDef* l_Date)
{
	I2C_Write(DS3231_ADDRESS_DATE, (uint8_t*)l_Date, 3);
}
//------------------------------------------------

