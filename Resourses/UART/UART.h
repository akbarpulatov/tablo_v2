#ifndef __UART_H
#define __UART_H

#include "mydeff.h"
#include "stm32f1xx.h"


void DMA_Init(DMA_Channel_TypeDef* DMA_Channel);
void UART_Init(USART_TypeDef* UART);
void Uart_Send(USART_TypeDef* Uart, uint8_t* data, uint16_t len);
void Uart_Receive(USART_TypeDef* Uart, uint8_t* data, uint16_t len);

void Uart_Send_Dma(uint8_t *data, int32_t len);


#endif // !__UART_H
