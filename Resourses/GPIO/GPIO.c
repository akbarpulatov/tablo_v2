#include "stm32f1xx.h"
#include "GPIO.h"
//------------------------------------------------
void GPIO_Init(void)
{
	RCC->APB2ENR |=  RCC_APB2ENR_IOPCEN
				 |	 RCC_APB2ENR_IOPAEN;
	
	//PC13
	GPIOC->CRH &= ~(GPIO_CRH_CNF13_Msk | GPIO_CRH_MODE13_Msk);
	GPIOC->CRH |= (0b11 << GPIO_CRH_MODE13_Pos) | (0b00 << GPIO_CRH_CNF13_Pos);
	
	//PC14
	GPIOC->CRH &= ~(GPIO_CRH_CNF14_Msk | GPIO_CRH_MODE14_Msk);
	GPIOC->CRH |= (0b11 << GPIO_CRH_MODE14_Pos) | (0b00 << GPIO_CRH_CNF14_Pos);
	
	//PC15
	GPIOC->CRH &= ~(GPIO_CRH_CNF15_Msk | GPIO_CRH_MODE15_Msk);
	GPIOC->CRH |= (0b11 << GPIO_CRH_MODE15_Pos) | (0b00 << GPIO_CRH_CNF15_Pos);
	
	
	//PA2 for External Interrupt
	RCC->APB2ENR |= RCC_APB2ENR_IOPAEN
				 |  RCC_APB2ENR_AFIOEN;
	//PA2
	GPIOA->CRL &= ~(GPIO_CRL_CNF2_Msk | GPIO_CRL_MODE2_Msk);
	GPIOA->CRL |= (0b00 << GPIO_CRL_MODE2_Pos) | (0b10 << GPIO_CRL_CNF2_Pos);
	GPIOA->ODR |= 1 << GPIO_ODR_ODR2_Pos;
	
	//EXTI2 for PA
	AFIO->EXTICR[0] |= AFIO_EXTICR1_EXTI2_PA;
	//Falling Edge
	EXTI->FTSR |= EXTI_FTSR_TR2;
	//Setting Up Mask
	EXTI->IMR |= EXTI_IMR_MR2;
	//Permit Interrupt On Line EXTI2
	NVIC_EnableIRQ(EXTI2_IRQn);
	
}
//------------------------------------------------
