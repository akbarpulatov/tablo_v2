#ifndef __MYDEFF_H
#define __MYDEFF_H

#define TEST
#define myDEBUG

#define bp				asm("BKPT")
#define delaypolling(n) for (uint32_t notusedvar = 0; notusedvar < n; notusedvar++)

// Defines of Peripherals
#define UART_DEBUG		USART1
#define DMA_UART		DMA1_Channel4
#define DMA				DMA1

#define SPI_ENC			SPI1



#define RCLK0			GPIOA->BSRR = GPIO_BSRR_BR8
#define RCLK1			GPIOA->BSRR = GPIO_BSRR_BS8


//=========================== Flags & Settings ======================================================================
#define	Tick					Flags.b0
#define Synchro					Flags.b1
#define FlagTemperMeas			Flags.b2
#define I2CREADFLAG				Flags.b3

#define shiftTimems				ucRegSettings[0]
#define TemperSource			ucRegSettings[1]
//=========================== Timers ======================================================================
#define	Timer_Shift				msTMR1
#define Timer_Pause				msTMR2
#define DelDS18					msTMR3
#define DelResDS18				msTMR4
#define Del_Temp				msTMR5

//========================= CONSTANTS ==================================================================================
#define BUFFERSIZE				86
#define NumRows					8
#define NumCols					6	// 6 bytes => 6x8 = 48individual leds per line
#define MODBUSID				0x11

//Temperature source
#define TemperFromModBus		0
#define TemperFromSens			1

#define REG_INPUT_START		1000	// Adress boshlanishi,
#define REG_INPUT_NREGS		1		//virtualniy tasavvurdagi registrlar soni

#define REG_TIME_START		2000
#define REG_TIME_NREGS		6

#define REG_TEMP_START		2006
#define REG_TEMP_NREGS		1

#define REG_TEXT_START		3000
#define REG_TEXT_NREGS		64

#define REG_FORMAT_START	5000
#define REG_FORMAT_NREGS	64

/*
 *Settings: 
 *0-Register = DelayTimeShift=40(default)
 *1-Register = Choose if(temperature from MODBUD/internal)  0 = ModBus; 1 = TEMP Sensor
 **/
#define REG_SETTINGS_START	7000
#define REG_SETTINGS_NREGS	30

#define REG_FONT_START	8000
#define REG_FONT_NREGS	1968



#define CopyString(s1, s2, Num)								\
			for(BYTE t = 0 ; t < Num ; t++)						\
			{													\
				s1[t] = s2[t];								\
			}		

//=========================== Pin Assignment ======================================================================
//#define LEDG					PCout(13)
#define DEB						PCout(14)
#define LEDG					PCout(13)
#define ErrorLED				PAout(1)


#define Ser_DS					PBout(15)
#define Ser_SH					PBout(13)
#define Ser_ST					PAout(8)
#define RCLK					PAout(8)

#define invID_bit0				PAin(15)
#define invID_bit1				PBin(3)
#define invID_bit2				PBin(4)
//#define invID_bit3				PAin(4)
#define invID_bit3				(1)
#define invID_bit4				PBin(6)
#define invID_bit5				PBin(7)
#define invID_bit6				PBin(8)
#define invID_bit7				PBin(9)

#define ID_bit0					!invID_bit0
#define ID_bit1					!invID_bit1
#define ID_bit2					!invID_bit2
#define ID_bit3					!invID_bit3
#define ID_bit4					!invID_bit4
#define ID_bit5					!invID_bit5
#define ID_bit6					!invID_bit6
#define ID_bit7					!invID_bit7



//======================================================================================================================
typedef unsigned char			uchar;
typedef unsigned int			uint;

typedef unsigned char			uint8_t;
typedef unsigned char           u8;    		// 8-bit unsigned
typedef unsigned short int      u16;  		// 8-bit unsigned
typedef unsigned int            u32;  		// 8-bit unsigned

typedef unsigned char           BYTE;    		// 8-bit unsigned
typedef unsigned short int      WORD;    		// 16-bit unsigned
typedef unsigned int            UINT;    		// 32-bit unsigned
typedef unsigned long           DWORD;    		// 32-bit unsigned
typedef unsigned long long      QWORD;    		// 64-bit unsigned
typedef signed char             CHAR;    		// 8-bit signed
typedef signed short int        SHORT;    		// 16-bit signed
typedef signed long             LONG;    		// 32-bit signed
typedef signed long long        LONGLONG;    	// 64-bit signed

//#define bool _Bool
//#define BOOL bool	
#define true 1
#define false 0

//======================================================================================================================
#define Nop() asm("NOP")
#define Reset()		NVIC_SystemReset()
//======================================================================================================================
#define BITBAND(addr, bitnum) ((addr & 0xF0000000)+0x2000000+((addr &0xFFFFF)<<5)+(bitnum<<2)) 
#define MEM_ADDR(addr)  *((volatile unsigned long  *)(addr)) 
#define BIT_ADDR(addr, bitnum)   MEM_ADDR(BITBAND(addr, bitnum)) 
//----------------------------------------------------------------------------------------------------------------------
#define GPIOA_ODR_Addr    (GPIOA_BASE+12) //0x4001080C 
#define GPIOB_ODR_Addr    (GPIOB_BASE+12) //0x40010C0C 
#define GPIOC_ODR_Addr    (GPIOC_BASE+12) //0x4001100C 
#define GPIOD_ODR_Addr    (GPIOD_BASE+12) //0x4001140C 
#define GPIOE_ODR_Addr    (GPIOE_BASE+12) //0x4001180C 
#define GPIOF_ODR_Addr    (GPIOF_BASE+12) //0x40011A0C    
#define GPIOG_ODR_Addr    (GPIOG_BASE+12) //0x40011E0C    

#define GPIOA_IDR_Addr    (GPIOA_BASE+8) //0x40010808 
#define GPIOB_IDR_Addr    (GPIOB_BASE+8) //0x40010C08 
#define GPIOC_IDR_Addr    (GPIOC_BASE+8) //0x40011008 
#define GPIOD_IDR_Addr    (GPIOD_BASE+8) //0x40011408 
#define GPIOE_IDR_Addr    (GPIOE_BASE+8) //0x40011808 
#define GPIOF_IDR_Addr    (GPIOF_BASE+8) //0x40011A08 
#define GPIOG_IDR_Addr    (GPIOG_BASE+8) //0x40011E08 
//----------------------------------------------------------------------------------------------------------------------
#define PAout(n)   BIT_ADDR(GPIOA_ODR_Addr,n)  //
#define PAin(n)    BIT_ADDR(GPIOA_IDR_Addr,n)  //

#define PBout(n)   BIT_ADDR(GPIOB_ODR_Addr,n)  //
#define PBin(n)    BIT_ADDR(GPIOB_IDR_Addr,n)  //

#define PCout(n)   BIT_ADDR(GPIOC_ODR_Addr,n)  //
#define PCin(n)    BIT_ADDR(GPIOC_IDR_Addr,n)  //

#define PDout(n)   BIT_ADDR(GPIOD_ODR_Addr,n)  //
#define PDin(n)    BIT_ADDR(GPIOD_IDR_Addr,n)  //

#define PEout(n)   BIT_ADDR(GPIOE_ODR_Addr,n)  //
#define PEin(n)    BIT_ADDR(GPIOE_IDR_Addr,n)  //

#define PFout(n)   BIT_ADDR(GPIOF_ODR_Addr,n)  //
#define PFin(n)    BIT_ADDR(GPIOF_IDR_Addr,n)  //

#define PGout(n)   BIT_ADDR(GPIOG_ODR_Addr,n)  //
#define PGin(n)    BIT_ADDR(GPIOG_IDR_Addr,n)  //
//======================================================================================================================
#define DelayNop10() 			Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop()



//========================= Структуры ==================================================================================
#if defined(__CC_ARM)
#pragma anon_unions
#endif

typedef enum
{
	NOERROR, 		//No Error
	MDInitError		//Modbus Initialization Error
} ErrorID;

typedef union _Byte
{
	BYTE _byte;
	struct
	{
		unsigned b0 : 1;
		unsigned b1 : 1;
		unsigned b2 : 1;
		unsigned b3 : 1;
		unsigned b4 : 1;
		unsigned b5 : 1;
		unsigned b6 : 1;
		unsigned b7 : 1;
	};
} Byte;


typedef union _Word
{
	WORD _word;
	BYTE v[2];
	struct
	{
		unsigned b0 : 1;
		unsigned b1 : 1;
		unsigned b2 : 1;
		unsigned b3 : 1;
		unsigned b4 : 1;
		unsigned b5 : 1;
		unsigned b6 : 1;
		unsigned b7 : 1;
		unsigned b8 : 1;
		unsigned b9 : 1;
		unsigned b10 : 1;
		unsigned b11 : 1;
		unsigned b12 : 1;
		unsigned b13 : 1;
		unsigned b14 : 1;
		unsigned b15 : 1;
	};
} Word;

typedef union _Dword
{
	DWORD _dword;
	BYTE v[4];
	struct
	{
		unsigned b0 : 1;
		unsigned b1 : 1;
		unsigned b2 : 1;
		unsigned b3 : 1;
		unsigned b4 : 1;
		unsigned b5 : 1;
		unsigned b6 : 1;
		unsigned b7 : 1;
		unsigned b8 : 1;
		unsigned b9 : 1;
		unsigned b10 : 1;
		unsigned b11 : 1;
		unsigned b12 : 1;
		unsigned b13 : 1;
		unsigned b14 : 1;
		unsigned b15 : 1;
		unsigned b16 : 1;
		unsigned b17 : 1;
		unsigned b18 : 1;
		unsigned b19 : 1;
		unsigned b20 : 1;
		unsigned b21 : 1;
		unsigned b22 : 1;
		unsigned b23 : 1;
		unsigned b24 : 1;
		unsigned b25 : 1;
		unsigned b26 : 1;
		unsigned b27 : 1;
		unsigned b28 : 1;
		unsigned b29 : 1;
		unsigned b30 : 1;
		unsigned b31 : 1;
	};
} Dword;

typedef struct
{
	BYTE  YY;
	BYTE  MM;
	BYTE  DD;
	BYTE  HH;
	BYTE  MIN;
	BYTE  SS;
	WORD  ZZZ;
}TimeTypeDef;

typedef struct
{
	unsigned char Seconds;
	unsigned char Minutes;
	unsigned char Hours;
}RTC_TimeTypeDef;

typedef struct
{
	unsigned char Date;
	unsigned char Month;
	unsigned char Year;
}RTC_DateTypeDef;

typedef struct
{
	unsigned char SEC;
	unsigned char MIN;
	unsigned char HOUR;
	unsigned char DAY;
	unsigned char DATE;
	unsigned char MONTH;
	unsigned char YEAR;
	unsigned char ALRM1_SEC;
	unsigned char ALRM1_MIN;
	unsigned char ALRM1_HOUR;
	unsigned char ALRM1_DAY_DATE;
	unsigned char ALRM2_MIN;
	unsigned char ALRM2_HOUR;
	unsigned char ALRM2_DAY_DATE;
	unsigned char CNTRL;
	unsigned char CNTRL_STATS;
	unsigned char AGINGOFFSET;
	unsigned char TEMP_MSB;
	unsigned char TEMP_LSB;
} DS_3231_typedef;

typedef enum 
{
	PRELASTBYTE,
	LASTBYTE
} stateSpiEnd;

typedef struct {
	uint8_t len;
	uint8_t counter;
	uint8_t txdata[20];
	uint8_t rxdata[20];
} typedef_i2c;

//======================================================================================================================
#endif // !__MYDEFF_H
