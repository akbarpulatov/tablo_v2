#include "Timers.h"
#include "stm32f1xx.h"

WORD msTMR_Del = 0;
DWORD msTMR1;
DWORD msTMR2;
DWORD msTMR3;
DWORD msTMR4;
DWORD msTMR5;
DWORD msTMR6;
DWORD msTMR7;
DWORD msTMR8;

DWORD CountTick = 0;
DWORD Count10ms = 0;
DWORD Count100ms = 0;
//------------------------------------------------------
void SYSCLK_Init(void)
{
	//SysClolk = 72 MHz, => PLL = x9, SOURCE = PLL
	//Flash Latency corresponding to 72MHz SysClk
	FLASH->ACR |= (0b010 << FLASH_ACR_LATENCY_Pos);
	
	//Turn on HSE and wait until ready
	SET_BIT(RCC->CR, RCC_CR_HSEON); 
	while (!(RCC->CR & RCC_CR_HSERDY)) ;
	
	//Turn on PLL and wait until ready
	RCC->CFGR |= (0b0111 << RCC_CFGR_PLLMULL_Pos) | RCC_CFGR_PLLSRC;
	RCC->CR |= RCC_CR_PLLON;
	while (!(RCC->CR & RCC_CR_PLLRDY)) ;
	
	//AHB, APB2, APB1 prescalers
	RCC->CFGR |= (0 << RCC_CFGR_HPRE_Pos)
			  |  (0 << RCC_CFGR_PPRE2_Pos)
			  |  (0b100 << RCC_CFGR_PPRE1_Pos);
	RCC->CFGR |= RCC_CFGR_SW_PLL;
	while (!(RCC->CFGR & RCC_CFGR_SWS_PLL)) ;
	
	//select PA8(should be configured seperately) as SYSclock output
	//	MODIFY_REG(RCC->CFGR, RCC_CFGR_MCO, RCC_CFGR_MCOSEL_SYSCLK);
	//	
	//	RCC->APB2ENR |= RCC_APB2ENR_IOPAEN;
	//	//PA8 = MCO
	//	GPIOA->CRH &= ~(GPIO_CRH_CNF8 | GPIO_CRH_MODE8_Msk);
	//	GPIOA->CRH |= (0b11 << GPIO_CRH_MODE8_Pos) | (0b10 << GPIO_CRH_CNF8_Pos);
}
//------------------------------------------------------
void Timer2_Init(void)
{
	//Peripheral Clock Enable
	RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;
	while (!(RCC->APB1ENR & RCC_APB1ENR_TIM2EN)) ;
	NVIC_EnableIRQ(TIM2_IRQn);
	TIM2->PSC = 100 - 1;
	TIM2->ARR = 720 - 1;
	
	//Update Interrupt Enable
	TIM2->DIER |= TIM_DIER_UIE;
	
	//Counter Enable
	TIM2->CR1 |= TIM_CR1_CEN;
}
//------------------------------------------------------
void DelayNop(void)
{
	Nop(); Nop(); Nop(); Nop(); Nop(); Nop(); Nop(); Nop();
	Nop(); Nop(); Nop(); Nop(); Nop(); Nop(); Nop(); Nop();
	Nop(); Nop(); Nop(); Nop(); Nop(); Nop(); Nop(); Nop();
	Nop(); Nop(); Nop(); Nop(); Nop(); Nop(); Nop(); Nop();
	Nop(); Nop(); Nop(); Nop(); Nop(); Nop(); Nop(); Nop();
	Nop(); Nop(); Nop(); Nop(); Nop(); Nop(); Nop(); Nop();
	Nop(); Nop(); Nop(); Nop(); Nop(); Nop(); Nop(); Nop();
	Nop(); Nop(); Nop(); Nop(); Nop(); Nop(); Nop(); Nop();
}
//------------------------------------------------------
void TIM2_IRQHandler(void)
{
	TIM2->SR &= ~TIM_SR_UIF;
	
	if (msTMR1)	msTMR1--;
	if (msTMR2)	msTMR2--;
	if (msTMR3)	msTMR3--;
	if (msTMR4)	msTMR4--;
	if (msTMR5)	msTMR5--;
	if (msTMR6)	msTMR6--;
	if (msTMR7)	msTMR7--;
	if (msTMR8)	msTMR8--;
}
//------------------------------------------------------




